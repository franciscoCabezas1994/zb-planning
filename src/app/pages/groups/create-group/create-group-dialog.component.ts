import {Component, EventEmitter, Inject} from '@angular/core';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';

import {DIALOG_DATA, IZbDialogData, ZbDialogRef} from '@zb/suite-core';

import {ICurriculumJournals} from '../models/ICurriculumJournals';

@Component({
  templateUrl: './create-group-dialog.component.html',
  styleUrls: ['./create-group-dialog.component.scss'],
})
export class CreateGroupDialogComponent {

  form: CreateGroupForm;

  constructor(
    @Inject(DIALOG_DATA) public dialogData: IZbDialogData,
    public dialogRef: ZbDialogRef<CreateGroupDialogComponent>
  ) {
    const {years, periods, programTypes, programs, curriculums, levels, journals} = this.dialogData;

    this.dialogData.years = years || [];
    this.dialogData.periods = periods || [];
    this.dialogData.programTypes = programTypes || [];
    this.dialogData.programs = programs || [];
    this.dialogData.curriculums = curriculums || [];
    this.dialogData.levels = levels || [];
    this.dialogData.journals = journals || [];
    this.dialogData.yearChange = new EventEmitter<IFormChanged>();
    this.dialogData.periodChange = new EventEmitter<IFormChanged>();
    this.dialogData.programTypeChange = new EventEmitter<IFormChanged>();
    this.dialogData.programChange = new EventEmitter<IFormChanged>();
    this.dialogData.curriculumChange = new EventEmitter<IFormChanged>();
    this.dialogData.create = new EventEmitter<IFormChanged>();

    this.form = new CreateGroupForm();
    this.form.controls.year.valueChanges.subscribe(value => this.dialogData.yearChange.emit({value, form: this.form}));
    this.form.controls.period.valueChanges.subscribe(value => this.dialogData.periodChange.emit({value, form: this.form}));
    this.form.controls.programType.valueChanges.subscribe(value => this.dialogData.programTypeChange.emit({value, form: this.form}));
    this.form.controls.program.valueChanges.subscribe(value => this.dialogData.programChange.emit({value, form: this.form}));
    this.form.controls.curriculumId.valueChanges.subscribe(value => {
      const qLeveles = this.dialogData.curriculums.find(c => c.id === +value)?.levels || 0;

      this.dialogData.levels = Array.from({length: qLeveles}, (_, i) => i + 1);
      this.dialogData.curriculumChange.emit({value, form: this.form});
    });
  }

  create(): void {
    this.form.markAllAsTouched();

    if (this.form.valid) {
      this.dialogData.create.emit({value: this.form.getRawValue(), form: this.form});
    }
  }

  cancel(): void {
    this.dialogRef.close(this.form.getRawValue());
  }
}

class CreateGroupForm extends FormGroup {

  get groups(): GroupsArrayForm {
   return this.controls.groups as GroupsArrayForm;
  }

  constructor() {
    super({
      period: new FormControl(0, [Validators.required]),
      year: new FormControl(0, [Validators.required]),
      programType: new FormControl(0, [Validators.required]),
      program: new FormControl(0, [Validators.required]),
      curriculumId: new FormControl(0, [Validators.required]),
      level: new FormControl(0, [Validators.required]),
      groups: new GroupsArrayForm([]),
    });
  }

  getRawValue(): any {
    const raw = super.getRawValue();

    // removing unnecesary properties
    delete raw.programType;
    delete raw.program;

    // parsing properties
    raw.year = +raw.year;
    raw.period = +raw.period;
    raw.curriculumId = +raw.curriculumId;
    raw.level = +raw.level;

    return raw;
  }

  setJournals(journals: ICurriculumJournals[]): void {
    this.setControl('groups', new GroupsArrayForm(journals));
  }
}

class GroupsArrayForm extends FormArray {

  get forms(): FormGroup[] {
    return this.controls as FormGroup[];
  }

  constructor(journals: ICurriculumJournals[]) {
    super(journals.map(j => new FormGroup({
      name: new FormControl(j.name),
      capacity: new FormControl(0),
      quantityGroups: new FormControl(0),
      studyDayId: new FormControl(j.id),
    })));
  }

  getRawValue(): any[] {
    return super.getRawValue().map(v => {
      delete v.name;
      return v;
    });
  }
}

interface IFormChanged {
  value: any;
  form: AbstractControl;
}

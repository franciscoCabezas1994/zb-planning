import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IListResponse } from 'src/app/models/list-response';
import { environment } from 'src/environments/environment';
import { IGroup } from './models/IGroup';
import { ISubject } from './models/ISubject';
import {ITypeProgram} from "./models/ITypeProgram";
import {IProgram} from "./models/IProgram";
import {ICurriculum} from "./models/ICurriculum";
import {IPeriod} from "./models/IPeriod";
import {ICurriculumJournals} from "./models/ICurriculumJournals";

@Injectable({ providedIn: 'root' })
export class GroupService {
  private baseUrl = `${environment.baseUrl}planning/groups/`;

  constructor(private http: HttpClient) {}

  createGroup(element: any): Observable<any> {
    return this.http.post(`${this.baseUrl}`, element);
  }

  getGroups(): Observable<IListResponse<IGroup>> {
    const fakeBody = {
      year: 2021,
      period: 2,
      curriculum: 1,
      program: 1,
      levels: [1, 2, 3],
      studyDays: [1],
    };
    return this.http.post<IListResponse<IGroup>>(
      `${this.baseUrl}search?pageNumber=0&pageSize=20`,
      fakeBody
    );
  }

  getSubjects(groupId: number): Observable<ISubject[]> {
    return this.http.get<ISubject[]>(`${this.baseUrl}${groupId}/subjects`);
  }

  updateSubjects(subjects: any[], groupId: number): Observable<any> {
    return this.http.put(`${this.baseUrl}${groupId}/subjects`, { subjects });
  }

  getTeachers() {
    return this.http.get(`${this.baseUrl}teachers/search`);
  }

  getYears(): Observable<number[]> {
    return this.http.get<number[]>(`${environment.baseUrl}core/academic-calendars/years`);
  }

  getPeriods(): Observable<number[]> {
    return this.http.get<number[]>(`${environment.baseUrl}core/academic-calendars/periods`);
  }

  getProgramTypes({year, period}: IProgramTypeParams): Observable<ITypeProgram[]> {
    return this.http.get<ITypeProgram[]>(`${environment.baseUrl}core/academic-calendars/years/${year}/periods/${period}/program-types`);
  }

  getPrograms({year, period, programType}: IProgramsParams): Observable<IProgram[]> {
    return this.http.get<IProgram[]>(`${environment.baseUrl}core/academic-calendars/years/${year}/periods/${period}/program-types/${programType}/programs`);
  }

  getCurriculums(programId: number): Observable<ICurriculum[]> {
    return this.http.get<ICurriculum[]>(`${environment.baseUrl}core/curriculums/active?programId=${programId}`);
  }

  getJournals(curriculumId: number): Observable<ICurriculumJournals[]> {
    return this.http.get<ICurriculumJournals[]>(`${environment.baseUrl}core/academic-offers/curriculums/${curriculumId}/journals`);
  }

}

interface IProgramTypeParams {
  year: number;
  period: number;
}

interface IProgramsParams {
  year: number;
  period: number;
  programType: number;
}

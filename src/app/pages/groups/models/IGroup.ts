import { ISubject } from './ISubject';

export interface IGroup {
  id: number;
  year: number;
  period: number;
  level: number;
  curriculumId: number;
  studyDayId: number;
  programId: number;
  code: string;
  curriculumName: string;
  studyDayName: string;
  scheduleStatus: string;
  studyDayColor: string;
  groups: string;
  subjects?: ISubject[];
  active: boolean;
}

import {IProgram} from './IProgram';

export interface ICurriculum {
  code: string;
  id: number;
  levels: number;
  name: string;
  program: IProgram;
}

export interface ICurriculumJournals {
  id: number;
  code: string;
  name: string;
  starAt: string;
  endAt: string;
  color: string;
  status: string;
}

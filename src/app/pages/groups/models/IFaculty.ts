export interface IFaculty {
  code: string;
  id: number;
  name: string;
  status: string;
}

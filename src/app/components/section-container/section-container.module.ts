import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SectionContainerComponent } from './section-container.component';

@NgModule({
  declarations: [SectionContainerComponent],
  exports: [SectionContainerComponent],
  imports: [CommonModule],
})
export class SectionContainerModule {}
